﻿﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Quartz;
 using Yes.CreditApplication.Api.Client;

 namespace Scheduler.Domain.Jobs
{
    [DisallowConcurrentExecution]
    public class DeleteExpiredCreditApplicationsJob : IJob
    {
        private readonly ILogger<DeleteExpiredCreditApplicationsJob> logger;
        private readonly IMaintenanceCreditApplicationClient client;

        public DeleteExpiredCreditApplicationsJob(ILogger<DeleteExpiredCreditApplicationsJob> logger, IMaintenanceCreditApplicationClient client)
        {
            this.logger = logger;
            this.client = client;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                await client.DeleteExpiredCreditApplications();
                logger.LogDebug($"{GetType().Name} executed");
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Failed to execute {GetType().Name}");
            }
        }
    }
}