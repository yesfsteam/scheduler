﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Spi;
using Scheduler.Models;

namespace Scheduler.Domain.Quartz
{
    public class QuartzHostedService : IHostedService
    {
        private readonly ISchedulerFactory schedulerFactory;
        private readonly IJobFactory jobFactory;
        private readonly List<JobConfiguration> jobConfigurations;

        public QuartzHostedService(ISchedulerFactory schedulerFactory, IJobFactory jobFactory, List<JobConfiguration> jobConfigurations)
        {
            this.schedulerFactory = schedulerFactory;
            this.jobConfigurations = jobConfigurations;
            this.jobFactory = jobFactory;
        }

        public IScheduler Scheduler { get; set; }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Scheduler = await schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = jobFactory;

            foreach (var jobSchedule in jobConfigurations.Where(x => x.Enabled))
            {
                var job = createJob(jobSchedule);
                var trigger = createTrigger(jobSchedule);

                await Scheduler.ScheduleJob(job, trigger, cancellationToken);
            }

            await Scheduler.Start(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (Scheduler != null)
                await Scheduler.Shutdown(cancellationToken);
        }

        private static IJobDetail createJob(JobConfiguration jobConfiguration)
        {
            var jobType = Type.GetType(jobConfiguration.Name);
            return JobBuilder
                .Create(jobType)
                .WithIdentity(jobType.FullName)
                .WithDescription(jobType.Name)
                .Build();
        }

        private static ITrigger createTrigger(JobConfiguration jobConfiguration)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{jobConfiguration.Name}.trigger")
                .WithCronSchedule(jobConfiguration.Schedule)
                .WithDescription(jobConfiguration.Name)
                .Build();
        }
    }
}
