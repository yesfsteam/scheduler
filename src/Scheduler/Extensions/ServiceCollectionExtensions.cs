﻿﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.Models;

namespace Scheduler.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddJob<T>(this IServiceCollection services) where T: class
        {
            services.AddSingleton<T>();
            services.AddSingleton(s => 
                s.GetService<List<JobConfiguration>>().FirstOrDefault(x => x.Name == typeof(T).Name));
            return services;
        }
    }
}
