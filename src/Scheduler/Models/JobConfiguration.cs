﻿﻿namespace Scheduler.Models
{
    public class JobConfiguration
    {
        public string Name { get; set; }
        public string Schedule { get; set; }
        public bool Enabled { get; set; }
    }
}
