using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Scheduler.Domain.Jobs;
using Scheduler.Domain.Quartz;
using Scheduler.Extensions;
using Scheduler.Models;
using Serilog;
using Yes.CreditApplication.Api.Client;
using Yes.Infrastructure.Http.Extensions;

namespace Scheduler
{
    public class Startup
    {
	    private readonly string applicationName;
	    
	    public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = Assembly.GetExecutingAssembly().GetName().Name;
        }

        private readonly IConfiguration configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);

            
            //configure Quartz
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddHostedService<QuartzHostedService>();

            //configure jobs
            var jobs = new List<JobConfiguration>();
            configuration.Bind("Jobs", jobs);
            services.AddSingleton(jobs);
            services.AddJob<DeleteExpiredCreditApplicationsJob>();

            services.AddHttpClientFromConfiguration<IMaintenanceCreditApplicationClient, MaintenanceCreditApplicationClient>(
	            configuration, "CreditApplicationApi");
            
            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));
	            });
           
			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
		}

        public void Configure(IApplicationBuilder app, IHostApplicationLifetime applicationLifetime)
        {
	        applicationLifetime.ApplicationStopping.Register(OnApplicationStopping);

	        app.UseRouting();
	        app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

	        if (configuration.GetValue<bool>("EnableSwagger"))
	        {
		        app.UseSwagger();
		        app.UseSwaggerUI(c =>
		        {
			        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1");
			        c.RoutePrefix = string.Empty;
		        });
	        }
            
	        Log.Logger.Information($"{applicationName} has been started");
        }

        private void OnApplicationStopping()
        {
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}